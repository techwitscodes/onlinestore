<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Initial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('menu_products', function (Blueprint $table){
            $table->increments('mpid');
            $table->integer('uid');
		    $table->string('slug',191)->uinque();
		    $table->integer('catid');
		    $table->integer('price'); // only naira prices would be stored
		    $table->string('name');

		    $table->string('manufacturer')->nullable();
		    $table->string('designer')->nullable();
		    $table->string('made_in')->nullable();

		    $table->string('material')->nullable();
		    $table->string('key_features',4000)->nullable();
		    $table->string('instructions')->nullable();
		    $table->timestamps();

	    });

        Schema::create('menu_product_images',function (Blueprint $table){
		    $table->increments('mpimid');
		    $table->integer('mpid');
            $table->string('url');
            $table->integer('uid');
		    $table->timestamps();
        });

        Schema::create('comments',function (Blueprint $table){
		    $table->increments('cid');
            $table->string('title');
            $table->string('body');
            $table->integer('pid');
            $table->integer('uid');
            $table->integer('rid');
		    $table->timestamps();
	    });

        
        Schema::create('reviews',function (Blueprint $table){
		    $table->increments('rid');
		    $table->integer('quantity');
		    $table->timestamps();
	    });

	    Schema::create('settings',function (Blueprint $table){
		    $table->increments('setid');
		    $table->string('name');
		    $table->string('value');
		    $table->timestamps();
	    });

	    Schema::create('categories',function (Blueprint $table){
		    $table->increments('catid');
		    $table->string('name');
		    $table->string('description');
		    $table->timestamps();
	    });

	 

	    Schema::create('sales',function (Blueprint $table){
		    $table->increments('sid');
		    $table->integer('uid');
		    $table->string('address');
		    $table->string('total');
		    $table->string('payment_method');
		    $table->timestamps();
	    });

	    Schema::create('sale_details',function (Blueprint $table){
		    $table->increments('sdid');
		    $table->integer('sid');
		    $table->integer('mpid');
		    $table->integer('quantity');
		    $table->timestamps();
	    });


	    Schema::create('payments',function (Blueprint $table){
		    $table->increments('payid');
		    $table->integer('sid');
		    $table->string('amount');
		    $table->string('reference');
		    $table->string('method')->default('paystack');
		    $table->string('others')->nullable();
		    $table->timestamps();
	    });


	    



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('menu_products');
        Schema::dropIfExists('menu_product_images'); 
        Schema::dropIfExists('comments');
        Schema::dropIfExists('reviews');
        Schema::dropIfExists('settings');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('sales');
        Schema::dropIfExists('sales_details');
        Schema::dropIfExists('payments');
    }
}
